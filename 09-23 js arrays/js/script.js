// Darbinis objektų masyvas, kuriame saugomi įrašai
let cars = [];
// Lentelė, į kurią atvaizduojamas mašinų registras
let printCarsTable = document.getElementById('cars_table');
// Laukiamas naujo įrašo įkėlimo mygtuko paspaudimas
let btnNew = document.getElementById('btn_register_new');
btnNew.addEventListener('click', registerCar);

let btnClear = document.getElementById('btn_clear');
btnClear.addEventListener('click', clearLocalStorage);



// Laukiama, kol užsikrauna HTML dokumentas ir atspausdinamas localstorage saugomas turinys
document.addEventListener("DOMContentLoaded", function(event) {
    cars = getFromStorage();
    if (cars === null) {
        cars = [];
    }
    printCars(cars, printCarsTable);
  });


// Laukiama paspaudimų ant dinaminių JS sukurtų elementų (rikiavimas)
document.body.addEventListener( 'click', function ( event ) {
    if (event.srcElement.id == 'sort_Brand') { cars = sortBrand (cars); printCars(cars, printCarsTable);
        } else if (event.srcElement.id == 'sort_Model') {  cars = sortModel (cars); printCars(cars, printCarsTable);
        } else if (event.srcElement.id == 'sort_Engine') {  cars = sortEngine (cars); printCars(cars, printCarsTable);
    }
  } );

// Įrašų įkėlimas iš localStorage
function getFromStorage () {
    let carsArray = [];
    if (typeof(Storage) !== "undefined") {
        let text = localStorage.getItem("carArray");
        if (text === "") { return carsArray; }
        carsArray = JSON.parse(text);
        return carsArray;
    } else {
       return carsArray;
    }
}

// Masyvo pateikimas lentelėje
function printCars(carsArray, carsTable) {
    
    if (carsArray.length>0) {
        
        //carsTable.innerHTML = `
        let t_head = `
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Brand <i id="sort_Brand" class="fas fa-sort"></i></th>
                                <th>Model <i id="sort_Model" class="fas fa-sort"></i></th>
                                <th>Engine <i id="sort_Engine" class="fas fa-sort"></i></th>
                                <th>Remove</th>
                            </tr>
                        </thead> 
                        `;
        let t_body = `<tbody>`;
        
        carsArray.forEach(function(car, index) {
            t_body += `
                <tr>
                    <td>${car.Id}</td>
                    <td>${car.Brand}</td>
                    <td>${car.Model}</td>
                    <td>${car.Engine}</td>
                    <td><span car="${car.Id}" class="btn btn-danger btn-xs btn-delete-car"  >X</span></td>
                </tr>
                `;
        });
        t_body += `</tbody>`;
        carsTable.innerHTML = t_head + t_body;
    } else { carsTable.innerHTML="None"; }
}

//Duomenų gavimas iš formos, gražina įrašo tipo elementą
function getFromForm (forId) {
    let brand = document.getElementById('brand').value;
    let model = document.getElementById('model').value;
    let engine = document.getElementById('engine').value;

    if (!brand || !model || !engine) {
        alert('Fill in all text areas first.');
        return false;
    } 
    if (isNaN(engine)) {
        alert('Engine must be a valid number.');
        return false;
    } else { engine = Number(engine); }

    let car = {
        Id: forId, 
        Brand: brand,
        Model: model,
        Engine: engine
    }
    return car;
}
$("#carForm").submit(function (event) {
    event.preventDefault();
    
    registerCar();
});

$("#test").click(function (event) {    
    console.log("test buvo paspaustas");
});

$("#cars_table").on('click', '.btn-delete-car', function() {
    console.log(".btn-delete-car click()");
    let carId = $(this).attr("car");
    alert();
    $(this).text("I was clicked!");

});

$("#class").click(function () {
    alert();
});

//Naujo automobilio įregistravimas
function registerCar(){
    //Duomenų iš teksto laukelių nuskaitymas ir patalpinimas į masyvą bei localstorage
    // cars.push(getFromForm(cars.length));
    cars.push(getFromForm(getAvailableID(cars)));
    localStorage.setItem("carArray", JSON.stringify(cars));
    //Duomenų įvedimo formos reset
    document.getElementById("carForm").reset;
    //Duomenų atvaizdavimas lentelėje
    printCars(cars, printCarsTable);
}

function getAvailableID (array) {
    var arrayOfId = array.map(car => car.Id);
    var i=0;
    for (i=0; i<arrayOfId[arrayOfId.length-1]; i++) {
        if (i+1 != arrayOfId[i]) { return i+1; }
    }
    return i+1;
}

function sortBrand (array) {
    array.sort(function(a, b) {
        var nameA = a.Brand.toUpperCase(); // ignore upper and lowercase
        var nameB = b.Brand.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;        }      
        // names must be equal
        return 0;
    });
    return array;
}

function sortModel (array) {
    array.sort(function(a, b) {
        var nameA = a.Model.toUpperCase(); // ignore upper and lowercase
        var nameB = b.Model.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;        }      
        // names must be equal
        return 0;
    });
    return array;
}

function sortEngine (array) {
    array.sort(function (a, b) {
        return a.Engine - b.Engine;
    });
    return array;
}

function clearLocalStorage() {
    cars = [];
    localStorage.setItem("carArray", cars);
    printCars(cars, printCarsTable);
    alert("Data is deleted.")
}

function removeCar (Id) {
    // let carToBeDeleted = cars.filter(function(temp_car){
    //     return temp_car.Id === Id;
    // })[0];
    // // let carToBeDeleted = cars.filter(temp_car => temp_car.Id === ID);

    var indexOfCar = cars.findIndex(c => c.Id === Id);

    cars.splice(indexOfCar,1);
    localStorage.setItem("carArray", JSON.stringify(cars));
    printCars(cars, printCarsTable);
}


